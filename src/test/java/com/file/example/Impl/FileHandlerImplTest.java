package com.file.example.Impl;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by masiaban on 6/27/17.
 */
public class FileHandlerImplTest {
    @Test
    public void sortContentFile() throws Exception {
        FileHandlerImpl fileHandler = new FileHandlerImpl();
        fileHandler.sortContentFile();
        ArrayList<Integer> firstAssertArray = new ArrayList<>(Arrays.asList(5, 6, 10, 12, 13));
        Assert.assertEquals(fileHandler.getFirstNum(), firstAssertArray);
        ArrayList<Integer> secondAssertArray = new ArrayList<>(Arrays.asList(23, 11, 9, 7, 2));
        Assert.assertEquals(fileHandler.getSecondNum(), secondAssertArray);
    }
}