package com.file.example.ExceptionHandler;

import spi.services.exhandler.AbstractException;
import spi.services.exhandler.ExceptionLevel;
import spi.services.exhandler.Interface.HandleAble;

/**
 * Created by masiaban on 6/26/17.
 */
public class ExceptionFileHandler extends AbstractException {
    public ExceptionFileHandler(ExceptionLevel level, String errorMessage, String errorCode, String businessModule, Throwable cause, HandleAble handleAble, Boolean handleAsync) {
        super(level, errorMessage, errorCode, businessModule, cause, handleAble, handleAsync);
    }

    public ExceptionFileHandler(String message) {
        super(ExceptionLevel.COMMON, message, null, null, null, null, false);
//test
    }
}
