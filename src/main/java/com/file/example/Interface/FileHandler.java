package com.file.example.Interface;

import com.file.example.ExceptionHandler.ExceptionFileHandler;

import java.io.IOException;

/**
 * Created by masiaban on 6/26/17.
 */
public interface FileHandler {
    void sortContentFile() throws IOException, ExceptionFileHandler;
}
