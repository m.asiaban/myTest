package com.file.example.Impl;

import com.file.example.ExceptionHandler.ExceptionFileHandler;
import com.file.example.Interface.FileHandler;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collections;

public class FileHandlerImpl implements FileHandler {

    private ArrayList<Integer> firstNum = new ArrayList<>();
    private ArrayList<Integer> secondNum = new ArrayList<>();

    @Override
    public void sortContentFile() throws ExceptionFileHandler {
        String[] contentArray = null;
        try {
            String fileName = "numericData.txt";
            ClassLoader classLoader = new FileHandlerImpl().getClass().getClassLoader();
            File file = new File(classLoader.getResource(fileName).getFile());

            String content = new String(Files.readAllBytes(file.toPath()));
            contentArray = StringUtils.split(content, "\n");
        } catch (Exception ex) {
            throw new ExceptionFileHandler("Reading File failed.!");
        }

        try {
            for (String singleLine : contentArray) {
                String[] tempSplitter = StringUtils.split(singleLine, ",");
                firstNum.add(Integer.parseInt(tempSplitter[0]));
                secondNum.add(Integer.parseInt(tempSplitter[1]));
            }
            Collections.sort(firstNum);
            Collections.sort(secondNum);
            Collections.reverse(secondNum);


        } catch (Exception ex) {
            throw new ExceptionFileHandler("Data type mismatch.!");
        }
    }

    public ArrayList<Integer> getFirstNum() {
        return firstNum;
    }

    public void setFirstNum(ArrayList<Integer> firstNum) {
        this.firstNum = firstNum;
    }

    public ArrayList<Integer> getSecondNum() {
        return secondNum;
    }

    public void setSecondNum(ArrayList<Integer> secondNum) {
        this.secondNum = secondNum;
    }
}


